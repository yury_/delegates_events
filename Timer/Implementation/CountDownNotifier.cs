﻿using System;
using System.Threading;
using Timer.EventArgs;
using Timer.Interfaces;

namespace Timer.Implementation
{
	public class CountDownNotifier : ICountDownNotifier
	{
		private Action<string, int> _startedDelegate;
		private Action<string, int> _tickedDelegate;
		private Action<string> _stoppedDelegate;

		private readonly Timer _timer;

		private int _counter;

        public CountDownNotifier(Timer timer)
        {
			_timer = timer;
			_counter = _timer.Ticks;
        }
		public void Init(
			Action<string, int> startedDelegate,
			Action<string> stoppedDelegate,
			Action<string, int> tickedDelegate)			
		{
			
			_startedDelegate = startedDelegate;
			_stoppedDelegate = stoppedDelegate;
			_tickedDelegate = tickedDelegate;
			

			_timer.Started += timer_Started;
			_timer.Stopped += timer_Stopped;
			_timer.Ticked += timer_Ticked;
		}

        private void timer_Ticked(object sender, TickedEventArgs eventArgs)
        {
            if (_tickedDelegate == null)
            {
				return;
            }
            _tickedDelegate(eventArgs.Name, eventArgs.TimeLeft);
        }

        private void timer_Stopped(object sender, StoppedEventArgs eventArgs)
        {
            if (_stoppedDelegate == null)
            {
				return;
            }
            _stoppedDelegate(eventArgs.Name);
        }

        private void timer_Started(object sender, StartedEventArgs eventArgs)
        {
            if (_startedDelegate == null)
            {
				return;
            }
            _startedDelegate(eventArgs.Name, eventArgs.Ticks);
        }

        public void Run()
		{
			_timer.StartTimer();


            while (_counter > 1)
            {
				_counter = _counter - 1;
				Thread.Sleep(200);
				_timer.Continue(_counter);
			}
		

			_timer.StopTimer();
		}
		
	}
}