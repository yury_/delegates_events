﻿using System;

namespace Timer.Factories
{
    public class TimerFactory
    {
        public TimerFactory()
        {

        }
        public Timer CreateTimer(string name, int ticks)
        {
            var timer = new Timer(name, ticks);           

            return timer;
        }        
    }
}