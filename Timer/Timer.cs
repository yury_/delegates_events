﻿using System;
using Timer.EventArgs;

namespace Timer
{   
    public class Timer
    {
        public event EventHandler<StartedEventArgs> Started;
        public event EventHandler<TickedEventArgs> Ticked;
        public event EventHandler<StoppedEventArgs> Stopped;

        public string Name { get; private set; }
        public int Ticks { get; private set; }

        public Timer(string name, int ticks)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentException($"{nameof(name)}");
            }
            if (ticks <= 0)
            {
                throw new ArgumentException($"{nameof(ticks)}");
            }
            Name = name;
            Ticks = ticks;           
        }

        public void StartTimer()
        {
            Started?.Invoke(this, new StartedEventArgs(Name, Ticks));
        }

        public void Continue(int timeLeft)
        {
            Ticked?.Invoke(this, new TickedEventArgs(Name, timeLeft));
        }

        public void StopTimer()
        {
            Stopped?.Invoke(this, new StoppedEventArgs(Name));
        }
    }
}