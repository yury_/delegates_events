﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Timer.EventArgs
{
	public class StartedEventArgs : System.EventArgs
	{
		public string Name { get; private set; }
		public int Ticks { get; private set; }

		public StartedEventArgs(string name, int ticks)
		{
			Name = name;
			Ticks = ticks;
		}
	}
}
