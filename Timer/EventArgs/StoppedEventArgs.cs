﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Timer.EventArgs
{
	public class StoppedEventArgs : System.EventArgs
	{
		public StoppedEventArgs(string name)
		{
			Name = name;
		}
		public string Name { get; private set; }

	}
}
