﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Timer.EventArgs
{
	public class TickedEventArgs : System.EventArgs
	{
		public string Name { get; private set; }
		public int TimeLeft { get; private set; }

		public TickedEventArgs(string name, int timeLeft)
		{
			Name = name;
			TimeLeft = timeLeft;
		}
	}
}
